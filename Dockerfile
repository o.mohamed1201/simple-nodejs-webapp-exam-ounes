FROM node:14

WORKDIR /app

COPY package*.json ./

RUN apt install

COPY . .

EXPOSE 8080

CMD ["node", "count-server.js"]
