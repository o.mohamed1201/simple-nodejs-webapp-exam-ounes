# Q1: Define what is a Microservices architecture and how does it differ from a Monolithic application.

Microservices are like independent building blocks for an application, each doing a small part of the job and talking to each other. This makes the application more flexible and easier to develop. On the other hand, a monolith is like a big building block where all parts of the application are connected. It's simpler at the beginning, but it gets complicated as the application grows.

# Q2: Compare Microservices and Monolithic architectures by listing their respective pros and cons.

## Microservices Pros and Cons : 

### Pros
- Scalability: Individual services can be scaled independently.
- Flexibility: Different services can be developed and deployed independently.
- Technology Diversity: Each microservice can use different technologies.
- Fault Isolation: Failures in one service don't affect the entire system.

### Cons
- Complexity: Managing a distributed system can be complex.
- Latency: Inter-service communication can introduce latency.
- Data Consistency: Maintaining consistency across services can be challenging.

## Monolithic Pros and Cons : 

### Pros
- Simplicity: Easier to develop and maintain.
- Performance: No overhead of inter-service communication.
- Synchronous Communication: Direct method calls for communication.

### Cons
- Scalability: Scaling requires duplicating the application.
- Technology Lock-in: Limited technology choices.
- Deployment Dependency: The application needs to be deployed at once.

# Q3: In a Micoservices architecture, explain how should the application be split and why.

In a Microservices architecture, the application should be split based on business capabilities. Each microservice should represent a specific business function or capability. This allows for independent development, deployment, and scalability of each service.

# Q4: Briefly explain the CAP theorem and its relevance to distributed systems and Microservices.

The CAP theorem says in a distributed system, you can't have all three: Consistency, Availability, and Partition Tolerance at the same time. In Microservices, during a network split, you have to pick between consistency and availability. This affects how systems deal with network problems and keep data consistent.

# Q5: What consequences on the architecture ?

So, like, the CAP theorem is this thing in Microservices that says you can't have it all - Consistency, Availability, and Partition Tolerance. It's like a tough choice during a network issue; you gotta pick between keeping things consistent or making sure everything stays available. Architects have to figure out this balance for the system to work right. It's kind of a big deal in how we design things.

# Q6: Provide an example of how microservices can enhance scalability in a cloud environment.

Imagine you have this online store. With microservices, each part like product search, checkout, and user profiles is a separate thing. If the checkout part gets super busy, you can just add more microservices for that bit without messing up the other stuff. So, microservices let you scale up the busy parts without blowing up the whole store. It's like having extra cash registers when there's a shopping frenzy, keeping things smooth in the cloud store.

# Q7: What is statelessness and why is it important in microservices architecture ?

Statelessness means that each microservice operates without retaining information about the state of other services. This is important in Microservices architecture as it promotes scalability and resilience. Stateless services are easier to scale horizontally, and failures in one instance don't affect others.

# Q8: What purposes does an API Gateway serve ?

 API Gateway is like the superhero of Microservices. It's the boss that manages all the requests coming in and sends them to the right microservice. It does cool stuff like checking who's allowed to come in, making sure everything's safe and sound. Plus, it spreads the load between the microservices, so no one gets overwhelmed. It's like the traffic cop for our Microservices neighborhood, keeping things organized and secure.
